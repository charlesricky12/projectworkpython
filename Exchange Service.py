import json
import requests

list_currency_json = requests.get('https://openexchangerates.org/api/' +
                                  'currencies.json?app_id=75f14df81559434eaf' +
                                  'b33294017c7f80/user').json()
conversion_base = requests.get("https://openexchangerates.org/api/" +
                                   "latest.json?app_id=75f14df81559434" +
                                   "eafb33294017c7f80").json()
base = None
to_base = None


def rates():
    while True:
        base_input = input("Enter the currency you want to convert from: ")
        if(base_input in list_currency_json):
            base = base_input
            print ("You have selected " + list_currency_json[base])
            break
        else:
            print("Error. The Currency you have selected" +
                  "is unsupported or not on our list")
    while True:
        to_input = input("Enter the currency you want to convert to: ")
        if(to_input in list_currency_json):
            to_base = to_input
            print ("You have selected " + list_currency_json[to_input])
            break
        else:
            print("Error. The Currency you have selected" +
                  "is unsupported or not on our list")
    convert = conversion_base['rates']
    rates = convert[to_input]/convert[base_input]
    print("The conversion rate is " + str(rates))
    return rates


def convert():
    go_ahead = True
    while go_ahead:
        try:
            print("=====NEW TRANSACTION======")
            rate = rates()
            base_value = float(input("Please enter the amount" +
                                     "you want to be converted: "))
            total = base_value * rate
            print ("The value of the exchange is " + str(total))
            cont = input("Another one? (yes/no) > ")
            while cont.lower() not in ("yes", "no"):
                cont = input("Another one? (yes/no) > ")
            if cont == "no":
                go_ahead = False
        except Exception:
            print("Invalid Input. Please try again")


convert()
